SHELL = /bin/bash

chart_path = ./chart/apache2-fcgi-proxy
chart_name = $(shell cat $(chart_path)/Chart.yaml | grep ^name | awk '{print $$2}')
chart_version = $(shell cat $(chart_path)/Chart.yaml | grep ^version | awk '{print $$2}')
chart_package = $(chart_name)-$(chart_version).tgz

build_tag ?= apache2-fcgi-proxy

.PHONY: build
build:
	docker build -t $(build_tag) - < ./Dockerfile

.PHONY: lint-chart
lint-chart:
	helm lint $(chart_path)

.PHONY: clean-chart
clean-chart:
	rm -f apache2-fcgi-proxy-*.tgz

$(chart_package): clean-chart
	helm package $(chart_path)

.PHONY: package-chart
package-chart: $(chart_package)

ifdef CI
.PHONY: helm-login
helm-login:
	helm registry login -u gitlab-ci-token -p $(CI_JOB_TOKEN) $(CI_REGISTRY)

.PHONY: chart
chart: $(chart_package) helm-login
	helm push $(chart_package) oci://$(CI_REGISTRY_IMAGE)/chart
endif
