FROM httpd:2.4

SHELL ["/bin/bash", "-c"]

LABEL org.opencontainers.artifact.title="Apache HTTPD 2.4 with FCGI proxy config"
LABEL org.opencontainers.artifact.description="Apache HTTPD 2.4 with FCGI proxy config for IIPImage Server"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/devops/containers/apache2-fcgi-proxy"
LABEL org.opencontainers.image.version="2.4"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.license="Apache-2.0"

# HTTPD_PREFIX is set upstream
RUN set -eux; \
    sed -i \
    -e 's/^Listen 80/Listen 8080/' \
    -e 's/^#\(LoadModule proxy_module modules\/mod_proxy.so\)/\1/' \
    -e 's/^#\(LoadModule proxy_connect_module modules\/mod_proxy_connect.so\)/\1/' \
    -e 's/^#\(LoadModule proxy_fcgi_module modules\/mod_proxy_fcgi.so\)/\1/' \
    $HTTPD_PREFIX/conf/httpd.conf; \
    echo 'ProxyPass / fcgi://${FCGI_HOST}:${FCGI_PORT}/' >> $HTTPD_PREFIX/conf/httpd.conf; \
    echo 'ProxyPassReverse / fcgi://${FCGI_HOST}:${FCGI_PORT}/' >> $HTTPD_PREFIX/conf/httpd.conf; \
    mkdir -p -m 0775 $HTTPD_PREFIX/logs; \
    chgrp -R 0 $HTTPD_PREFIX/logs; \
    chmod -R g=u $HTTPD_PREFIX/logs; \
    usermod -a -G 0 www-data

ENV FCGI_HOST="localhost" \
    FCGI_PORT="9000"

EXPOSE 8080

USER www-data
